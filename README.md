# Parking management system

<div align="center">
    <a href="https://www.php.net/releases/8.1/en.php" target="_blank">
        <img src="https://img.shields.io/badge/PHP-v8.1-777BB4?logo=PHP" alt="PHP version" title="PHP version">
    </a>
    <a href="https://laravel.com" target="_blank">
        <img src="https://img.shields.io/badge/Laravel Framework-v10-FF2D20?logo=Laravel" alt="Laravel version" title="Laravel version">
    </a>

</div>

## About

This is a parking management system for entry plate number of vehicles and exit them, also report for available parking lots for each zones. 
parking can has multiple zones with their capacity.

## Usage
Once the API is installed, you can start using it by sending HTTP requests to the following endpoints:

| Endpoint                        | Method   | Description                                               |
|:--------------------------------| :------- |:----------------------------------------------------------|
| `/zones/available-parking-lots` | `GET` | View available zones parking lots to use for parking      |
| `/parking/entry`                | `POST` | Enter vehicle plate number and zone for parking lot       |
| `parking/{parkingLot}`           | `PUT` | Exit vehicle from parking                                 |
| `/auth/login`                   | `POST` | Login a user                                              |
| `/auth/register`                | `POST` | Register a new user                                       | |



## Development

Clone project locally and do the following steps:

1. Install **mysql** v8, **composer** v2 and **php cli** v8.1 (with its extensions)
2. Navigate into the root of the project and copy `.env.example` as `.env`. Fill your local config in `.env` file.
3. Install dependencies with:
    ```shell script
    composer install
    ```
5. Generate an app key using artisan command:
    ```shell script
    php artisan key:generate
    ```
6. Run migrations:
    ```shell script
    php artisan migrate
    ```
7. Run Seeder for generate fake data:
    ```shell script
    php artisan db:seed
    ```
8. Start development server:
    ```shell script
    php artisan serve
    ```
9.  Features are on branch Assignment for easy code review.
10. Now you can use this section that include a Postman collection for interacting with the project's
   API.  [Download Local Postman Collection](./postman_collection/parking-system.postman_collection.json)
